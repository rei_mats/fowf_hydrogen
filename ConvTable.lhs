
--* Start
--** Import

> module ConvTable where

> import Data.List
> import Data.List.Split        
> import Text.Printf


--** Note

    Using this module, one can create table from values or functions.
    In this module, create [[String]] from function or values first and
    convert it to String. 
    One can configure string type.

--* Data / Type


> data Form a = Default 
>             | FormFunction (a->String)
> data StrMatrix = Rows [[String]] 
>                | Cols [[String]] 
>                  deriving (Show)

> data TableType = SimpleTable
> data TableData a b c  = Function (a->b->c) |
>                        Values [[c]]


--* Utils
--** map to [[x]], (x->y)->[[x]]->[[y]]

> mapMatrix :: (x->y) -> [[x]] -> [[y]]
> mapMatrix f xss = map (\xs -> map f xs) xss

--** direct product, [a]->[b]->[[(a,b)]]

> directProduct :: [a] -> [b] -> [[(a,b)]]
> directProduct as bs = splitEvery
>                       (length bs)
>                       [(a, b) | a<- as, b<-bs]

--* Create strMatrix 
--** create [[c]] from function
          
> fromFuncToValMatrix :: (a->b->c)->[a]->[b]->[[c]]
> fromFuncToValMatrix f as bs = mapMatrix g (directProduct as bs)
>     where g (a, b) = f a b
 
--** c->String

> toString :: (Show c) => Form c -> c -> String
> toString Default c = show c
> toString (FormFunction f) c = f c

--** format function

> maybeFormat :: (PrintfArg a) => String -> Maybe a -> String
> maybeFormat fmt (Just a) = printf fmt a
> maybeFormat _ Nothing    = ""

--** create strMatrix

> createStrMatrix :: (Show a, Show b, Show c) => Form a -> Form b -> Form c -> [a] -> [b] -> [[c]] -> StrMatrix
> createStrMatrix fa fb fc as bs css =
>     let dataMat = mapMatrix (toString fc) css
>         rowLbls = map (toString fa) as
>         fullMat = zipWith (:) rowLbls  dataMat
>         header  = "" : (map (toString fb) bs)
>     in Rows $ header:fullMat


 > createStrMatrix Default Default Default [1..2] [1..3] [[0.1,0.2,0.3],[0.0,0.1,0.2]]



--* Re Shape
--** make same length :: [String] -> [String]
--*** [String]     

> makeSameLength :: [String] -> [String]
> makeSameLength as = let n = maximum $ map length as
>                         f str = str ++ (replicate (2 + n - length str) ' ')
>                     in  map f as

--*** [[String]]

> sameLengthStrMatrix' :: [[String]] -> [[String]]
> sameLengthStrMatrix' css = map makeSameLength css

--*** StrMatrix

> sameLengthStrMatrix :: StrMatrix -> StrMatrix
> sameLengthStrMatrix (Cols css) = Cols $ sameLengthStrMatrix' css
> sameLengthStrMatrix (Rows css) =
>     let f = transpose.sameLengthStrMatrix'.transpose
>     in Rows $ f css 
    
 > sameLengthStrMatrix $ createStrMatrix Default Default Default [1..2] [1..3] [[0.1,0.2,0.3],[0.0,0.1,0.2]]


--** simple table

> simpleTable :: StrMatrix -> String
> simpleTable (Rows (cs:css)) = 
>     let f (str:strList) = str ++ "|" ++ (concat strList) ++ "\n"
>         header = (f cs)
>         bar  = replicate (length header) '-' ++ "\n"
>         dat  = concat $ map f css
>     in header ++ bar ++ dat
> simpleTable (Cols css) = simpleTable $ Rows $ transpose css



--* Interface     
--** simple table (old)

 > convTable' :: (Show a, Show b, Show c) => TableType -> Form a -> Form b -> Form c -> [a] -> [b] -> TableData a b c -> String
 > convTable' SimpleTable fa fb fc as bs (Function f)=
 >     let css    = fromFuncToValMatrix f as bs
 >         strMat = createStrMatrix fa fb fc as bs css
 >         sameLeng = sameLengthStrMatrix strMat
 >     in  simpleTable sameLeng


 > putStrLn $ convTable' SimpleTable  Default Default Default [1,2,3] [0.1,0.2,0.3] (Function (+))


--** simple table with options
--*** opt type

> data ConvTableOpts a b c = RowForm (Form a) |
>                            ColForm (Form b) |
>                            EleForm (Form c) 

--*** get opt

> convTableOptsOne :: (Form a, Form b, Form c) -> ConvTableOpts a b c ->
>                      (Form a, Form b, Form c)
> convTableOptsOne (fa, fb, fc) x = case x of
>                                          RowForm f -> (f, fb, fc)
>                                          ColForm f -> (fa,f , fc)
>                                          EleForm f -> (fa,fb, f)
>                                          _ -> (fa, fb, fc)


> convTableOpts :: [ConvTableOpts a b c] -> (Form a, Form b, Form c)
> convTableOpts xs = foldl convTableOptsOne (Default, Default, Default) xs


--*** main 

> convTable :: (Show a, Show b, Show c) => 
>              TableType -> [ConvTableOpts a b c] -> 
>              [a] -> [b] -> TableData a b c -> String
> convTable SimpleTable opts as bs (Function f) =
>     let (fa, fb, fc) = convTableOpts opts
>         css          = fromFuncToValMatrix f as bs
>         strMat       = createStrMatrix fa fb fc as bs css
>         sameLeng     = sameLengthStrMatrix strMat
>     in simpleTable sameLeng

 > putStrLn $ convTable SimpleTable []  [1,2,3] [0.1,0.2,0.3] (Function (+))

 > putStrLn $ convTable SimpleTable [RowForm (Function (\a->"xx"++show a))]  [1,2,3] [0.1,0.2,0.3] (Function (+))
      
 > putStrLn $ convTable SimpleTable [RowForm $ Function $ ("xx"++) . show ]  [1,2,3] [0.1,0.2,0.3] (Function (+))


--*** old
 > getRow :: ConvTableOpts a b c -> Form a
 > getRow (RowForm f) =  f
 > getRow _         = Default
 > getCol :: ConvTableOpts a b c -> Form b
 > getCol (ColForm f) = f
 > getCol _         = Default
 > getEle :: ConvTableOpts a b c -> Form c
 > getEle (EleForm f) = f
 > getEle  _        = Default

> {-
> getRow :: ConvTableOpts a b c -> Maybe (Form a)
> getRow (RowForm f) =  Just f
> getRow _         = Nothing
> getCol :: ConvTableOpts a b c -> Maybe (Form b)
> getCol (ColForm f) = Just f
> getCol _         = Nothing
> getEle :: ConvTableOpts a b c -> Maybe (Form c)
> getEle (EleForm f) = Just f
> getEle  _        = Nothing
> -}

> {-
> getRowForm :: [ConvTableOpts a b c] -> Form a
> getRowForm opts = let rows = filter (/= Nothing) (map getRow opts)
>                   in if length rows == 0 then
>                      Default else
>                      (\(Just x) -> x) $ head rows
> -}

> {-
> getColForm :: ConvTableOpts a b c -> Maybe (Form b)
> getColForm ColForm f = Just f
> getColForm _  = Nothing 
> getEleForm :: ConvTableOpts a b c -> Maybe (Form c)
> getEleForm EleForm f = Just f
> getEleForm _  = Nothing 
> -}

 > getForm :: [ConvTableOpts a b c] -> 
 > convtable :: (Show a, Show b, Show c) => 
 >              TableType -> [ConvTableOpts] -> 
 >              [a] -> [b] -> TableData a b c -> String
 > convtable SimpleTable opts as bs (Function f) =
 >     let fa = 


css = fromFuncToValMatrix f as bs

     
