--* Import / data / type
--** Module start

> module Fdm_complex where
         
--** Import
--*** hmatrix

> import Numeric.LinearAlgebra

--*** easy plot

> import Graphics.EasyPlot

--*** test

> import Test.QuickCheck
> import Test.HUnit

--*** Map/list

> import qualified Data.Map as M
> import Data.List
> import qualified Data.List.Split as DLS
> import Text.Printf

> import Control.Applicative

--** Data / type
--*** matrix and vector
      
      (Grid n h) : n is # of grid and h is grid width

> data Grid = Grid Int Double deriving (Show)

      complex function on one real variable

> type CFuncR1 = (Double -> Complex Double)

> type GridCF = (Grid, Vector (Complex Double))

> data BoundaryCond = Box |
>                     ConstLogDiff (Complex Double)

--*** Graphics

> type PlotData = [(Double, Double)]


--*** photoionization of H like atom

> data Channel = Ch1skp | Ch2pks deriving (Show)

> data Dipole  = Length | Velocity deriving (Show)

> data HLikePI = HLikePI {
>       channel :: Channel
>     , dipole  :: Dipole
>     , charge :: Double
>     , photonEnergy :: Double }
>                deriving (Show)

> data MethodForOugoingBC = CAP CFuncR1|
>                           OutgoingBC 

> instance Show (MethodForOugoingBC) where
>     show OutgoingBC = "Outgoing BC"
>     show (CAP _)    = "CAP"

--* Finite Difference Method for R+
--** grid from max range

> gridFromMaxRange :: Double -> Double -> Grid
> gridFromMaxRange maxR h = Grid (truncate (maxR / h)) h
     
--** gridToList
--*** Code

> gridToList :: Grid -> [Double]
> gridToList (Grid n h) = map ( (*h) . fromIntegral ) [1..n]

--*** unit test

> prop_gridToList n h =
>     n >  0 ==>
>     h >  0 ==>
>     h == h1
>     where h1 = head $ gridToList (Grid n h)
     

--** grid representation of f(r)

> gridCFuncR1List :: Grid -> CFuncR1 -> [Complex Double]
> gridCFuncR1List grid f = map f (gridToList grid)

--** laplacian matrix
--*** Code

> laplacianMat :: Int -> Matrix (Complex Double)
> laplacianMat n = fromRows $ map row [0..n-1] 
>     where row m
>               | m == 0    = assoc n 0.0 [(0, -2.0),  (1, 1.0)]
>               | m == (n-1)= assoc n 0.0 [(n-2, 1.0), (n-1, -2.0)]
>               | otherwise = assoc n 0.0 [(m-1, 1.0), (m, -2.0), (m+1, 1.0)]

--*** unit test

> test_laplacianMat :: Test
> test_laplacianMat =  test [ laplacianMat 3 ~?= exact]
>     where nt = (-2.0) :+ 0.0
>           l  = 1.0 :+ 0.0
>           o = 0.0 :+ 0.0
>           exact = fromLists [[nt, l, o], 
>                              [l ,nt, l],
>                              [o, l, nt]]

    interactive test

 > runTestTT test_laplacianMat
     
--** operator matrix representation
--*** Note

      Compute the operator matrix represented by grid. The boundary
      condition is 0 at origin and constant derivative log at asymptotic
      region.

--*** Code

> opMatrix :: Grid -> CFuncR1 -> BoundaryCond -> Matrix (Complex Double)
> opMatrix g@(Grid n _) f Box =
>     umat `add` kmat
>     where kmat = laplacianMat n
>           umat = (diag . fromList) (gridCFuncR1List g f)
> opMatrix g@(Grid n h) f (ConstLogDiff c) =
>     hmat `add` bmat
>     where hmat = opMatrix g f Box
>           v    = (2.0 * h :+ 0.0) * c
>           bmat = assoc (n, n) 0 [((n-1, n-2), 1), ((n-1,n-1), v)] :: Matrix (Complex Double)


--*** old code

> {-
> opMatrixLogDeri :: Grid -> CFuncR1 -> Complex Double -> Matrix (Complex Double)
> opMatrixLogDeri g@(Grid n h) f c =
>     umat `add` kmat `add` bmat
>     where kmat = laplacianMat n
>           umat = (diag .  fromList) (gridCFuncR1List g f)
>           v    = (2.0*h :+ 0.0) * c
>           bmat = assoc (n, n) 0 [((n-1, n-2), 1), ((n-1,n-1), v)] :: Matrix (Complex Double)
> -}

--*** interactive

 > let g = Grid 5 0.1
 > let f = (\r -> r :+ 0.0)
 > opMatrixLogDeri g f (0.0 :+ 2.0)
 
--** Solve
--*** Note

     The target equation is
         [d^2/dx^2 + u(x)]y(x) = f(x)

--*** Code
         
> solveR1 :: Grid -> CFuncR1 -> CFuncR1 -> BoundaryCond -> Vector (Complex Double)
> solveR1 g u f bc = head . toColumns $ linearSolve mat $ fromColumns [vec]
>     where mat = opMatrix g u bc
>           vec = fromList $ gridCFuncR1List g f

                


--* Graphics :: IO()
--** grid plot data

> gridPlotData :: Grid -> [Complex Double] -> (PlotData, PlotData)
> gridPlotData g zs = (zip xs rs, zip xs is)
>     where xs = gridToList g
>           rs = map realPart zs
>           is = map imagPart zs

--** plot of complex data

> plotComplexData :: TerminalType -> Grid -> [Complex Double] -> IO Bool
> plotComplexData term grid zs = plot term [
>                             Data2D [Title "Re", Color Blue] [] re,
>                             Data2D [Title "Im", Color Red] [] im]
>     where (re, im) = gridPlotData grid zs

--** CUI table

> printTable :: (Show a, Show b) => String -> String -> [(a, b)] ->IO ()
> printTable s t abList = 
>     do
>     putStrLn $ s ++ t
>     putStrLn $ unlines $ map (\(x, y) -> (show x) ++ (show y)) abList
           
 > putStrLn $ unlines $ map (\(x, y) -> x ++ y) [("a", "x"), ("b", "y"), ("c", "z")]


--* Photoionization of H like atom
--** Channel Information
--*** lQnum
               
> lQnum :: Channel -> Double
> lQnum Ch1skp = 1.0

--*** energy of initial bound state

> energyInitState :: Channel -> Double
> energyInitState Ch1skp = -0.5
> energyInitState Ch2pks = -1.0/8.0

--*** driven term

> drivenTerm :: Channel -> Dipole -> CFuncR1 
> drivenTerm Ch1skp Velocity = (\r -> r *     exp (-r) :+ 0.0)
> drivenTerm Ch1skp Length   = (\r -> 2 * r * r * exp (-r) :+ 0.0)
> drivenTerm Ch2pks Length   = (\r -> r * exp (-0.5 * r) :+ 0.0)

--** Potential
--*** Coulomb + angular

> potentialHLike :: HLikePI -> CFuncR1
> potentialHLike h = \r -> (-z/r + ll / (2 * r * r)) :+ 0.0
>     where z = charge h
>           l = lQnum (channel h)
>           ll= l * (l + 1)

--*** Complex absorbing potential

> powCAP :: Double -> Double -> Int -> CFuncR1
> powCAP mr a n = (\r -> if r > mr 
>                              then 0.0 :+ (-a * (r-mr) ^ n) 
>                              else 0.0 :+ 0.0)

--** Matrix (old)
--*** Outgoing boundary condition

> {-
> hLikeAtomMatOutgoingBC ::  Grid -> HLikePI -> Matrix (Complex Double)
> hLikeAtomMatOutgoingBC g@(Grid _ h)ha = opMatrix g f (ConstLogDiff ik)
>     where v   = potentialHLike ha
>           ene = (energyInitState (channel ha) + photonEnergy ha) :+ 0.0
>           d   = (-2) * h * h :+ 0.0
>           f   =  (*d) . (+ (-ene)) . v
>           ik  = i * sqrt (2*ene)
> -}

--** Vector (old)

> {-
> hLikeAtomPIVec :: Grid -> HLikePI -> Vector (Complex Double)
> hLikeAtomPIVec g h = fromList $ gridCFuncR1List g f
>     where f = ((-2)*) . (drivenTerm (channel h) (dipole h))
> -}
 
--** Solve
--*** Old

> {-
> solveHLikePIOld :: Grid -> HLikePI -> Vector (Complex Double)
> solveHLikePIOld g h = head . toColumns $ linearSolve mat $ fromColumns [vec]
>     where mat = hLikeAtomMatOutgoingBC g h
>           vec = hLikeAtomPIVec g h
> -}

--*** Code

> solveHLikePI :: Grid -> HLikePI -> MethodForOugoingBC -> Vector (Complex Double)
> solveHLikePI g@(Grid _ h) ha OutgoingBC =
>     solveR1 g u f (ConstLogDiff ik)
>     where d   = (-2) * h * h :+ 0.0
>           ene = (energyInitState (channel ha) +
>                 photonEnergy ha) :+ 0.0
>           u   = (*d) . (+ (-ene)) . potentialHLike ha
>           ik  = i * sqrt ( 2 * ene)
>           f   = (*d) . drivenTerm (channel ha) (dipole ha)


> solveHLikePI g@(Grid _ h) ha (CAP iv) =
>     solveR1 g u f Box
>     where d   = (-2) * h * h :+ 0.0
>           ene = (energyInitState (channel ha) + photonEnergy ha) :+ 0.0
>           u   = (*d) . (+ (-ene)) . (\r -> potentialHLike ha r + iv r)
>           f   = (*d) . drivenTerm (channel ha) (dipole ha)

--* Data analysis
--** complex func (x) at one point

     Comparing two complex number list and return most separate value

--*** old

 > getGridIndex :: Grid -> Double -> Maybe Int
 > getGridIndex (Grid _ h) x = 
 >     if abs (fromIntegral ki - kd) < 2 * eps then 
 >         Just $ ki - 1 else 
 >         Nothing
 >     where kd =  x / h
 >           ki = truncate kd :: Int
 
 > getGridCFuncAt' :: GridCF -> Double -> Maybe (Complex Double)
 > getGridCFuncAt' ((Grid _ h), vec) x =
 >     if abs (fromIntegral ki - kd) < 2 * eps then
 >        Just $ vec @> (ki-1) else
 >        Nothing 
 >     where kd = x / h
 >           ki = truncate kd :: Int
 
 > getGridCFuncAt'' :: GridCF -> Double -> Maybe (Complex Double)
 > getGridCFuncAt'' (g, vec) x = 
 >     if delta < 2 * eps then
 >        Just psi        else
 >        Nothing
 >     where ds = map (abs.(x-)) $ gridToList g
 >           (delta, psi) = head $ sortBy 
 >                          (\a-> \b-> compare (fst a) (fst b))
 >                          $ zip ds (toList vec)

--*** code

> sortPairByFst :: (Ord a) => [(a, b)] -> [(a,b)]
> sortPairByFst xys = sortBy comp xys
>     where comp x y = compare (fst x) (fst y)

> getGridCFuncAt :: GridCF -> Double -> Maybe (Complex Double)
> getGridCFuncAt (g, f) x =
>     let gs = map (abs.(x-)) $ gridToList g
>         fs = toList f
>         (delta, psi) = head $ sortPairByFst $ zip gs fs
>     in if delta < 2 * eps then
>        Just psi        else
>        Nothing

--*** old

 > diffGridCFuncAt :: GridCF -> GridCF -> Double -> Double
 > diffGridCFuncAt (x, a) (y, b) t = 

> maxDiffOfList :: [Complex Double] -> [Complex Double] -> Double
> maxDiffOfList a b = maximum $ map (realPart.abs) $ zipWith (-) a b

> maxDiffCFuncR1 :: Grid -> Double -> Vector (Complex Double) -> Vector (Complex Double) -> Double
> maxDiffCFuncR1 g maxR a b = maxDiffOfList (getSubVec a) (getSubVec b)
>     where num = length $ filter (<maxR) $ gridToList g
>           getSubVec v = take num $ toList v
          


--** difference of FOWF

> type CalcCond = (Grid, HLikePI,  MethodForOugoingBC)

> differenceAtPoint :: CalcCond -> CalcCond -> Double -> Maybe Double
> differenceAtPoint c1 c2 x = 
>     let f (g, h, c) = getGridCFuncAt (g, solveHLikePI g h c) x
>         v1 = f c1
>         v2 = f c2
>         err a b = realPart $ abs $ (a-b)/b
>     in (Just err) <*> v1 <*> v2

--** Table
--*** Note

      Compute value and construct table from these values

--*** Code

> adjustString :: Int -> String -> String
> adjustString n cs = if n > m then
>                    cs ++ replicate (n-m) ' ' else
>                    take n cs
>     where m = length cs

> constructValueTable :: (a->b->c)->[a]->[b]->[[c]]
> constructValueTable f as bs = DLS.splitEvery 
>                         (length bs)
>                         [f a b | a<-as, b<-bs]

> showOneLine :: (Show a, Show c) => Int -> a -> [c] -> String
> showOneLine n a cs = foldl (++) 
>                      ((adjustString n $ show a)  ++ "| ")
>                      (map (adjustString n . show) cs) ++ "\n"

> constructTable :: (Show a, Show b,Show c) => Int -> (a->b->c) -> [a] -> [b] -> String
> constructTable n f as bs = 
>     let 
>     css = constructValueTable f as bs
>     strs = zipWith (showOneLine n) as css
>     header = showOneLine n (replicate n ' ') bs
>     bar    = replicate ((n + 1) * length bs + 3) '-' ++ "\n"
>     in concat $ header:bar:strs


--* Interactive check
--** small

> hatom = HLikePI {
>           channel = Ch1skp
>         , dipole  = Length
>         , charge  = 1.0
>         , photonEnergy = 1.5}

> xgrid = Grid 3 0.1

 > hLikeAtomMat xgrid hatom

--** real

> xgridL :: Grid
> xgridL = Grid 500 0.1
> psivec :: Vector (Complex Double)         
> psivec = solveHLikePI xgridL hatom OutgoingBC
> psivecCAP = solveHLikePI xgridL hatom (CAP (powCAP 40.0 0.01 2))

 > plotComplexData X11 xgridL (toList psivec)
 > plotComplexData X11 xgridL (toList psivecCAP)

 > maxDiffCFuncR1 xgridL 30.0 psivec psivecCAP


--** driven term

> driv = drivenTerm (channel hatom) (dipole hatom)


 > plotComplexData X11 xgridL (toList (hLikeAtomPIVec xgridL hatom))




--** Data analysis

 > getGridCFuncAt (Grid 3 0.1, fromList [10.0,20.0,30.0]) 0.145
 > getGridCFuncAt (Grid 3 0.1, fromList [10.0,20.0,30.0]) 0.2

 > maxDiffCFuncR1 xgrid 3.0 (fromList [1.0, 2.0, 3.0]) (fromList [1.1, 2.0, 3.0])

 > let v1 = fromList [1, 2, 3, 4, 5, 6, 7, 8, 9 ] :: Vector Double
 > let v2 = fromList [1, 2, 3.1, 4, 5, 6, 7, 8, 9 ] :: Vector Double
 > maxDiffCFuncR1 (Grid 10 1.0) 6.0 () (fromList [1, 2, 3, 4, 5, 6, 7, 8, 9 ] :: Vector Double)


--* Calculation
--** Comparing result

     comparing the result
     row : max R
     col : h

> c1_row_h :: [Double]
> c1_row_h = [0.1, 0.2, 0.3, 0.5, 1.0]

> c1_col_maxR :: [Double]
> c1_col_maxR = [50.0, 100.0, 200]

> c1_func_sub_hatom :: HLikePI
> c1_func_sub_hatom = HLikePI {
>                     channel = Ch1skp
>                   , dipole  = Length
>                   , charge  = 1.0
>                   , photonEnergy = 1.5}

> c1_func_sub_psi :: Grid -> Maybe (Complex Double)
> c1_func_sub_psi g = let vec = solveHLikePI g c1_func_sub_hatom OutgoingBC
>                     in getGridCFuncAt (g, vec) 15.0

> c1_func_sub_base :: Maybe (Complex Double)
> c1_func_sub_base = c1_func_sub_psi (gridFromMaxRange 200.0 0.1)

> c1_func_main :: Double -> Double -> Maybe Double
> c1_func_main h r = let x = c1_func_sub_psi (gridFromMaxRange r h)
>                        y = c1_func_sub_base
>                    in (Just distance) <*> x <*> y
>     where distance a b = realPart $ abs $ (a-b) / b

> c1_table :: String
> c1_table = constructTable 28 c1_func_main c1_row_h c1_col_maxR

--** CAP vs Outgoing BC

> difCAPvsOutgoingBC = differenceAtPoint
>                      (gridFromMaxRange 200.0 0.1, hatom, CAP $ powCAP 190.0 0.1 2)
>                      (gridFromMaxRange 200.0 0.1, hatom, OutgoingBC) 15.0

 > difCAPvsOutgoingBC
         
--** Convergence of grid size (old)

     The convergence behaivior of grid size

> calc_gridValMap :: HLikePI -> Double -> [(Grid, MethodForOugoingBC)] ->  [Maybe (Complex Double)]
> calc_gridValMap ha x gms = map gridToVal gms
>     where gridToVal (g, m)= let psi = solveHLikePI g ha m
>                                 val = getGridCFuncAt (g, psi) x
>                             in val


> calc_gridComp :: HLikePI -> Double -> [(Grid, MethodForOugoingBC)] ->[Maybe (Double)]
> calc_gridComp ha x gms =
>     let vs = calc_gridValMap ha x gms     -- :: [Maybe (C Double)]
>         bv      = head vs                 -- ::  Maybe (C Double)
>     in  map (\v -> (Just dis) <*> v <*> bv) vs
>     where dis a b = realPart $ abs (a-b)

> gridComp_GridList :: [(Grid, MethodForOugoingBC)]
> gridComp_GridList = [(Grid (truncate $ r/h) h, m) |
>                      h<-hs, r<-rs,
>                      m <- [OutgoingBC, CAP $ powCAP (r * 0.8) 0.01 2]] 
>     where hs = [0.1, 0.2, 0.3, 0.5, 1.0]
>           rs = [100.0, 50.0]

 > printTable "a" "b" $ zip gridComp_GridList $ calc_gridComp hatom 15.0 gridComp_GridList 
     
> gridSizeComp_maxR     = 500.0
> gridSizeComp_gridMap :: M.Map Double Grid
> gridSizeComp_gridMap = M.fromList $ 
>                        map (\h -> (h, gridFromMaxRange gridSizeComp_maxR h))  
>                        [5.0, 1.0, 0.5, 0.1, 0.05, 0.01]
> gridSizeComp_key   = 0.01

> gridSizeComp_funcMap :: M.Map Double (Vector (Complex Double))
> gridSizeComp_funcMap = M.map
>                        (\g -> solveHLikePI g hatom OutgoingBC)
>                        gridSizeComp_gridMap


 > gridSizeComp_result :: M.Map Double Double
 > gridSizeComp_result = M.mapWithKey
 >                       (\g -> a -> maxDiffCFuncR1 g 30.0 a b)
 >                       gridSizeComp_funcMap
 >     where b = M.lookup gridSizeComp_key gridSizeComp_funcMap



  
  
