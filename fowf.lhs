{-# LANGUAGE UnicodeSyntax #-}

--* Import / data / type
--** Import
--*** Note
            
   Import Libraries.
   Graphics.EasyPlot needs "easyplot"
   
--*** Code

> import Numeric.LinearAlgebra
> import Graphics.EasyPlot 
> import Test.HUnit

--** data and type
--*** Note

      "Channel" describe photoionization channel of hydrogen atom.

--*** Code

> data Grid = Grid Int Double deriving (Show)
> type FuncR1 t = (Double -> t)
> data Channel = Ch1skp | Ch2pks
> data Dipole  = Length | Velocity

> type Charge = Double
> type PHEnergy  = Double
> type HLikePI = (Channel, Dipole, Charge, PHEnergy)

--* Utilities
--** disp
--*** Note
--*** Code

> disp :: Matrix Double -> IO ()
> disp = putStr . dispf 2

--** zerovec
--*** Note
      
      Return a list whose element is 0 and its length is n.
       
--*** Code

> zerovec :: (Num t) => Int -> [t]
> zerovec n = take n $ repeat 0

--** gridToList :: Grid -> [Double]
--*** Note

      Create List from Grid

--*** Code
      
> gridToList :: Grid -> [Double]
> gridToList (Grid n h) = map (*h) (take n [1..])

--** grid representation of F(r)
--*** Note

      Compute the grid representation vector of one varibale 
      complex function f.

--*** Code

> gridFuncR1 :: Grid -> FuncR1 (Complex Double) -> Vector (Complex Double)
> gridFuncR1 grid f = fromList $ map f (gridToList grid)

--** vector to plot data
--*** Note

      The input of Graphics.EasyPlot.plot is tuple list of num i.e
          [(1,2), (2,3), (3,4), ...].
      In this program, grid data are treated as "Vector Complex"
      and "Grid". 
      vectorToPlotData transform these data to tuple list
      
--*** Code

> vectorToPlotData :: Grid -> Vector (Complex Double) -> ([(Double, Double)], [(Double, Double)])
> vectorToPlotData grid vec =
>     (zip xs rs, zip xs is)
>     where xs = gridToList grid
>           zs = toList vec
>           rs = map realPart zs
>           is = map imagPart zs

--* Channel Information
--** l

> lQnum :: Channel -> Double
> lQnum Ch1skp = 1.0

--** E0

> energyInitState :: Channel -> Double
> energyInitState Ch1skp = -0.5
> energyInitState Ch2pks = -1.0/8.0


--* Graphics
--** plot f(x)
--*** Note
--*** Code

> plotComplexFunc :: TerminalType -> Grid -> Vector (Complex Double) -> IO Bool
> plotComplexFunc term grid vec = plot term [
>                             Data2D [Title "Re", Color Blue] [] re,
>                             Data2D [Title "Im", Color Red] [] im]
>     where (re, im) = vectorToPlotData grid vec

--* Compute Matrix
--** laplacian matrix row at i
--*** Note

      Compute raplacian matrix.
      The working equation is 

            y''(x) + f(x) y(x) = g(x)
            y(0) = 0
            y'/y (∞) = c

      Considering that grid size is h, discrete representation is 

           y_{j-1} - 2 y_{j} + y_{j+1} + h² f_{j} y_{j} = h²gⱼ


--*** Code

> laplacianMatRowi :: Int -> Complex Double ->  Int ->Vector (Complex Double)
> laplacianMatRowi n ch idx
>     | idx == 1  = fromList $ [-2, 1] ++ (zerovec $ n - 2)
>     | idx == n  = fromList $ (zerovec $ n - 2) ++ [2, -2 + 2*ch]
>     | otherwise = fromList $ 
>                   (zerovec $ idx - 2) ++
>                   [1,-2,1] ++ 
>                   (zerovec $ n - idx - 1)  
  
--** potential function
--*** Note
      
      Compute potential vector of hydrogen like atom.
      z is charge and l is angular quantum number 

--*** Code

> potentialHLike :: HLikePI -> FuncR1 (Complex Double)
> potentialHLike (ch, di, z, w) = \r -> (-z/r + l*(l+1)/(2*r*r)) :+ 0.0
>     where l = lQnum ch

--*** junk

 > let vr = potentialHLike (Ch1skp Length 1.0 1.5) 1.0
 > let xs = Grid 100 0.01
 > let vs = gridFuncR1 xs vr
 > plotComplexFunc (X11) xs vs

--** L matrix
--*** Note
--*** Code

> lmatrix :: HLikePI -> Grid -> Matrix (Complex Double)
> lmatrix hatom@(ch, _, _, w) grid@(Grid n h)  =
>     add umat kmat
>     where e0   = energyInitState ch
>           vvec = zipVectorWith (+)
>                  (gridFuncR1 grid (potentialHLike hatom))
>                  (constant ((-e0 - w) :+ 0.0) n)
>           mxhh2 = (-2*h*h) :+ 0.0
>           umat = diag $ mapVector (*mxhh2) vvec
>           ihk  = 0.0 :+ h * sqrt ( 2 * (e0 + w))
>           kmat = fromRows $ map (laplacianMatRowi n ihk) [1..n]

--*** junk

 > let xs    = Grid 3 1.0
 > let hatom = (Ch1skp, Length, 1.0, 1.5)
 > lmatrix hatom xs


--* Complex Matrix (New)
--** laplacian matrix
--*** Note

      Directory compute laplacian matrix with grid base.

--*** Code
      
> laplacianMat :: Int -> Matrix (Double)
> laplacianMat n = fromRows $ map row [0..n-1] 
>     where row m
>               | m == 0    = assoc n 0.0 [(0, -2.0),  (1, 1.0)]
>               | m == (n-1)= assoc n 0.0 [(n-2, 1.0), (n-1, -2.0)]
>               | otherwise = assoc n 0.0 [(m-1, 1.0), (m, -2.0), (m+1, 1.0)]

--*** Unit Test

> testLaplacianMat :: Test
> testLaplacianMat = test [ laplacianMat 3 ~?= exact]
>     where exact = fromLists [[-2.0, 1.0, 0.0], 
>                              [1.0, -2.0, 1.0],
>                              [0.0, 1.0, -2.0]] :: Matrix Double                    

--** plus potential and BC
--*** Note

      Obtain matrix representation

--*** Code

 > lmatrixWithBC ::  Grid -> (Double -> Complex Double) -> Complex Double -> Matrix (Complex Double)
 > lmatrixWithBC (Grid n h) u c = add $ umat tmat
 >     where umat = diag $ mapVector u (linspace n (0.0, h)) :: Matrix (Complex Double)
 >           tmat = laplacianMat n   :: Matrix (Complex Double)
 >           bmat = assoc (n, n) 0.0 [((n-1,n-1), c)]  :: Matrix (Complex Double)

 > lmatrixWithBC :: (Element a, Num a, Floating a) => Grid -> (Double -> a) -> a -> Matrix a
 > lmatrixWithBC (Grid n h) u c = add $ umat tmat
 >     where umat = diag $ mapVector u (linspace n (0.0, h)) :: Matrix a
 >           tmat = laplacianMat n  :: Matrix a
 >           bmat = assoc (n, n) 0.0 [((n-1,n-1), c)]  :: Matrix a

--* Compute Vector
--** Driven Term 
--*** Note

      Compute driven term radial function.
      
--*** Code

> drivenTerm :: Channel -> Dipole -> FuncR1 (Complex Double)
> drivenTerm Ch1skp Velocity = (\r -> r *     exp (-r) :+ 0.0)
> drivenTerm Ch1skp Length   = (\r -> r * r * exp (-r) :+ 0.0)
> drivenTerm Ch2pks Length   = (\r -> r * exp (-0.5 * r) :+ 0.0)

--** Grid Driven Term
--*** Note
      
       compute driven term represented by grid
       
--*** Code

> drivenTermGrid :: HLikePI -> Grid -> Vector (Complex Double)
> drivenTermGrid (ch, di, _, _) grid = gridFuncR1 grid f
>     where f = drivenTerm ch di

--*** junk

 > let xs = Grid 100 0.1
 > let psi = drivenTermGrid Ch1skp Length xs
 > plotComplexFunc xs psi

--* Solve 
--** Solve
--*** Note

      Compute grid based matrix and vector and solve linear system.

--*** Code

> solveHLikePI :: HLikePI -> Grid -> Matrix (Complex Double)
> solveHLikePI hatom grid =
>     linearSolve lmat $ fromColumns [driv]
>     where lmat = lmatrix hatom grid
>           driv = drivenTermGrid hatom grid

--*** junk

 > let xs = Grid 500 0.1
 > let hatom = (Ch1skp, Length, 1.0, 1.5)
 > let psis = solveHLikePI hatom xs
 > let vec = head $ toColumns psis
 > plotComplexFunc X11 xs vec

--* Unit Tests
--** junk test
--*** Note

      Practice for unit test

--*** Code

> tests :: Test
> tests = test [ 
>  null [] ~?= True,
>  True ~=? null [1],
>  zerovec 3 ~?= [0, 0, 0]]

--** zerovec
--*** Note

      Test for zerovec.
      Load this file to GHCi and lunch 
      runTestTT $ testsComputeMatrix
            
--*** Code

> testsComputeMatrix :: Test
> testsComputeMatrix = 
>     "compute matrix" ~: 
>     test [
>      "zerovec 3" ~:
>      zerovec 3 ~?= [0, 0, 0],
>      "zerovec 5" ~:
>      zerovec 5 ~?= [0, 0, 0]]




       
       
                
