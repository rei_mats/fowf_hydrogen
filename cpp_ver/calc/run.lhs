--** import

> import System.Process
> import System.IO
> import Data.Complex
> import Text.Printf
> import Control.Monad
 
--** environment setting
     
> type Width = Double
> type R_Max = Double
> type WaveNum = Double
> programName :: Bool -> WaveNum -> Width -> R_Max -> [Double] -> (String, [String])
> programName False k w r xs = let prog  = "../fowf"
>                                  opts1 = ["-n", 
>                                           "-w", show w, 
>                                           "-r", show r, 
>                                           "-k", show k]
>                                  opts2 = concat $ map (\x -> ["-x", show x]) xs
>                              in  (prog, opts1 ++ opts2)
> programName True k w r xs = let prog  = "../fowf"
>                                 opts1 = ["-n", 
>                                          "-w", show w, 
>                                          "-r", show r, 
>                                          "-k", show k]
>                                 opts3 = ["--complex_region_length", "20.0", "--amplitude", "0.1", "--power", "2"] 
>                                 opts2 = concat $ map (\x -> ["-x", show x]) xs
>                              in  (prog, opts1 ++ opts3 ++ opts2)

--** computation

> type FOWF_result = (Double, [Complex Double])

> readFOWFOutput :: String -> FOWF_result
> readFOWFOutput s = let (errLine:resLines) = lines s
>                        err   = (read.last.words) errLine :: Double
>                        datas = map read1 resLines
>                    in (err, datas)
>     where read1 :: String -> Complex Double
>           read1 s' =  let (_:rz:iz:_) = map read $ words s' :: [Double]
>                      in  rz :+ iz

> calc :: Bool -> WaveNum -> Width -> R_Max -> [Double] -> IO FOWF_result
> calc capQ k w r xs = do 
>               let (cmd, opts) = programName capQ k w r xs
>               (_, out, _, procHandle) <- runInteractiveProcess
>                                            cmd opts Nothing Nothing
>               outStr <- hGetContents out
>               let result = readFOWFOutput outStr
>               waitForProcess procHandle
>               return result                            

--** main
--*** show

> printOne :: Double -> Double -> FOWF_result -> String
> printOne w r (err, zs) = concat strs
>     where d = " &  "
>           strs = [show w, d, show r, d, show err] ++
>                  [d ++ (show $ realPart z) ++ " " ++ (show $ imagPart z) ++ "i" | z <- zs]
>                  ++ ["  \\\\"]

--*** main

> compute_var :: Bool -> [Double] -> WaveNum -> [Width] -> [R_Max] -> IO()
> compute_var capQ xs k hs rs = do
>                          putStrLn $ "k = " ++ show k
>                          when capQ $ putStr "Using CAP  : "
>                          putStrLn $ "h  & r &  err &  " ++ show xs
>                          sequence_ [
>                           do
>                           res <- calc capQ k h r xs
>                           putStrLn $ printOne h r res
>                           | h <- hs, r <- rs]
>                          return ()

                  
> main :: IO()
> main = do
>        let xs = [5, 10, 15, 20] :: [Double]
>            ks = [0.5, 1.0, 1.5]
>        forM_ ks 
>             (\k-> do
>                  compute_var False xs k [1.0, 0.5, 0.1, 0.05, 0.01] [200]
>                  putStrLn ""
>                  compute_var False xs k [0.01] [50, 100, 1000, 5000]
>                  putStrLn "")
>        return ()



