#include <iostream>
#include <Eigen/Sparse>
#include <boost/program_options.hpp>

using namespace std;
using namespace Eigen;
using namespace boost::program_options;
 
typedef complex<double> C;
typedef Triplet<C> T;
typedef SparseMatrix<C> M;
typedef VectorXcd V;
typedef vector<double> VD;

class ShowOptions {
public:
  bool showNormErr;
  bool showAll;
  vector<double>* targetPoints;
  ShowOptions(bool, bool, VD*);
  ~ShowOptions();
};
ShowOptions::ShowOptions(bool _showNormErr, bool _showAll,VD *_v) {
  showNormErr = _showNormErr;
  showAll     = _showAll;
  targetPoints  = new VD();
  copy(_v->begin(), _v->end(), back_inserter(*targetPoints));
}
ShowOptions::~ShowOptions() {
  delete targetPoints;
}

class FOWF {
private:
  M* D_mat;
  V* g_vec;
  V* x_vec;
  double h;
  int    n;
  double k;
  double complex_region_length;
  double cap_amp;
  int cap_pow;
  bool pose_outgoing_BC;
  double scaling_anlge;
  void allocate();
  void buildMatrix();
  void buildVector();
  double err ();
  C getValAt (double, double eps = 0.000001);
public:
  FOWF (double _h, double _r, double _k);
  void setCAP(int, double, double);
  void setScalingAngle(double, double);
  void buildProblem ();
  void solve ();
  void show(vector<double>* xs, bool showErrQ);
  void show(ShowOptions* opts);
  void showSetting();
};
void FOWF::allocate() {
  D_mat = new M(n, n);
  g_vec = new V(n);
  x_vec = new V(n);
}
void FOWF::buildMatrix() {
    vector<T> v;

    const double r0 = n*h - complex_region_length;
    const double rad = scaling_anlge * 3.1415 / 180.0;

    //    this->showSetting();

    auto F_es = [&](double x) {
      return x < r0 ? x : r0 + (x-r0) * exp(C(0.0, rad));
    };

    auto f_es = [&](double x) {
      return x < r0 ? 1.0 : exp(C(0.0, -rad));
    };
    
    auto u = [&](double x){ 
      const double l = 1.0, z = 1.0;
      const C xx = F_es(x);
      C res = -l*(l+1)/(xx*xx) + 2*z/xx + k*k;
      if (x > r0)
	res += 2.0 * C(0.0, 1.0) *cap_amp * pow(x-r0, cap_pow);
      return res; };

    // u = calc_u(h, k);
    v.push_back(T(0, 0, -2.0 + h*h*u(h)));
    v.push_back(T(0, 1, 1.0));

    for(int i = 1; i < n-1; i++) {
      double x = h*(i+1);
      C f_es2 = pow(f_es(x), 2);

      if( abs(x - r0) < h * 0.5) {
	C a = (exp(C(0.0, -rad)) - 1.0) * h / 2.0;
	v.push_back(T(i, i-1, 1.0 * f_es2 + a));
	v.push_back(T(i, i  , -2.0 * f_es2 + h*h*u(x)));	
	v.push_back(T(i, i+1, 1.0 * f_es2 - a));	
      } else {
	v.push_back(T(i, i-1, 1.0 * f_es2 ));
	v.push_back(T(i, i  , -2.0 * f_es2 + h*h*u(x)));	
	v.push_back(T(i, i+1, 1.0 * f_es2));
      }
    }

    double x = h * n;
    C f_es2 = pow(f_es(x), 2);
    if (this-> pose_outgoing_BC) {
      //      cout << "i am in outgoin BC" << endl;
      //      cout << "h, x, u(x), k" << h << x << u(x) << k << endl;
      v.push_back(T(n-1,n-2, 2.0 * f_es2));
      v.push_back(T(n-1,n-1, -2.0 * f_es2 +h*h*u(x) + C(0.0, 1.0) * 2.0*k*h));
    } else {
      v.push_back(T(n-1,n-2, +1.0 * f_es2));
      v.push_back(T(n-1,n-1, -2.0 * f_es2 + h*h*u(x)  ));
    }

    D_mat->setFromTriplets(v.begin(), v.end());

    //    cout << *D_mat << endl;
  }
void FOWF::buildVector() {
    auto g = [&](double x) { return -4 * x * x * exp(-x);} ;
  
    for(int i = 0; i < n; i++) {
      double x = (i+1) * h;
      (*g_vec)(i) = g(x) * h * h;
    }
  }
double FOWF::err () {return ((*D_mat)*(*x_vec)-(*g_vec)).norm(); }
C FOWF::getValAt (double x, double eps) {
  double x_div_h = x/h; 
  int    idx = round(x_div_h);
  if ( abs(x_div_h - idx) < eps) {
    return (*x_vec)(idx-1,0);
  } else {
    return 0.0;
  }}
FOWF::FOWF (double _h, double _r, double _k) {
  this->h = _h; 
  this->n = _r/_h;
  this->k = _k;
  this->allocate();
  this->pose_outgoing_BC = true;
  this->scaling_anlge = 0.0;
  this->complex_region_length = 0.0;
  this->cap_amp = 0.0;
  this->cap_pow = 0;
}
void FOWF::setCAP(int _pow, double _complex_region_length, double _amp) {
  this->complex_region_length = _complex_region_length;
  this->cap_amp = _amp;  
  this->cap_pow = _pow;
  this->pose_outgoing_BC = false;
}
void FOWF::setScalingAngle(double ang, double leng) {
  this->scaling_anlge = ang;
  this->complex_region_length = leng;
  this->pose_outgoing_BC = false;
}
void FOWF::buildProblem () {
  buildMatrix();
  buildVector();
}
void FOWF::solve () {
  SparseLU<M> solver;
  solver.analyzePattern(*D_mat);
  solver.factorize(*D_mat);
  *x_vec = solver.solve(*g_vec);
}
void FOWF::show(vector<double>* xs, bool showErrQ) {
  if (showErrQ) {
    cout << "(Ax-b).norm:  " << err() << endl;
  }
  
  for(vector<double>::iterator it = xs->begin(); 
      it != xs->end(); ++it) {
    const double x = *it;
    const C      y = getValAt(x);
    cout << x << "  " << real(y) << "  " << imag(y) << endl;
  }
}
void FOWF::show(ShowOptions* opts) {
  if (opts->showAll) {
    auto p = [](double x, C y) {
      cout << x << "  " << real(y) << "  " << imag(y) << endl;};
    double x;
    C      y;
    x = 0.0; y = 0.0;
    p(x, y);
    
    for (int i = 0; i < n; i++) {
      x = x + h; y = (*x_vec)(i, 0);
      p(x, y);
    }
  } else {
    show(opts->targetPoints, opts->showNormErr);
  }
}
void FOWF::showSetting() {
  cout << "width     : " << h << endl;
  cout << "# of grid : " << n << endl;
  cout << "wave num  : " << k << endl;
  cout << "complex region length" << complex_region_length << endl;
  cout << "cap amp" << cap_amp << endl;
  cout << "pose Outgoing BC" << pose_outgoing_BC << endl;
  cout << "scaling angle" << scaling_anlge << endl;
}

// read command line options and build objects of FOWF and ShowOptions
void setOptions(FOWF*& f, ShowOptions*& s, int argc, char *argv[]) {

    options_description opt("options");

    opt.add_options()
      ("help,h", "Display help")
      ("width,w", value<double>(), "grid width(1.0)")
      ("r_max,r", value<double>(), "maximum of grid span(10.0)")
      ("wave_num,k", value<double>(), "wave number(1.0)")
      ("eval_point,x", value<VD>(), "evaluating points")
      ("show_norm_error,n", "show norm err?")
      ("scaling_angle,s", value<double>(), "scaling angle of ECS(degree)")
      ("complex_region_length,crl", value<double>() , "complex region length (10.0)")
      ("amplitude,ca", value<double>(), "amplitude of CAP (0.0)")
      ("power,pow", value<int>(), "power of CAP (0)");


    // analyse
    variables_map argmap;
    store(parse_command_line(argc, argv, opt), argmap);
    notify(argmap);
    
    // show help
    if (argmap.count("help")) {
      cerr << opt << endl;
      exit(0);
    }
    
    // read value from options
    auto getVal = [&](string s, double x){
      double res= argmap.count(s) ? 
      argmap[s].as<double>() : x;
      return res;
    };
    
    const double h = getVal("width", 1.0);
    const double r = getVal("r_max", 10.0);
    const double k = getVal("wave_num", 1.0);
    const double c_len = getVal("complex_region_length", 10.0);
    const double ang = getVal("scaling_angle", 0.0);

    bool showNormErr = argmap.count("show_norm_error");
    
    VD targetPoints;
    if (argmap.count("eval_point"))
      targetPoints = argmap["eval_point"].as<VD>();

    // initialize
    f = new FOWF (h, r, k);
    bool showAll = targetPoints.size() == 0;
    s = new ShowOptions(showNormErr, showAll, &targetPoints) ;

    // set ECS
    if(argmap.count("scaling_angle")) 
      f->setScalingAngle(ang, c_len);

    // set CAP if exits power of amplitude options
    if (argmap.count("power") || argmap.count("amplitude")) {

      const double c_amp = getVal("amplitude", 0.0);

      const int c_pow = 
	argmap.count("power") ?
	argmap["power"].as<int>() : 0;

      f->setCAP(c_pow, c_len, c_amp);
    }
}
int main (int argc, char *argv[])
{
  FOWF *fowf;
  ShowOptions *opts;

  setOptions(fowf, opts, argc, argv);

  fowf->buildProblem();
  fowf->solve();

  fowf->show(opts);

  delete fowf;
  delete opts;
}
